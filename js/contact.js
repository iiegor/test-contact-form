
/**
 * Validates the format of a given email address
 * @param email
 * @return boolean
 */
function validateEmail(email) {
    let regex = new RegExp(/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/g);
    return regex.test(email);
}

let app = new Vue({
    el: '#app',

    data: {
        errors: [],
        firstName: null,
        lastName: null,
        phone: null,
        email: null,
        message: null,
        acceptTerms: false,
        success: false,
    },

    watch: {
        email(value) {
            this.email = value;

            if (!validateEmail(this.email)) {
                this.errors['email'] = 'Your email address must be in the format of name@domain.com';
            } else {
                delete this.errors['email'];
            }
        },

        acceptTerms(value) {
            this.acceptTerms = value;

            if (!this.acceptTerms) {
                this.errors['acceptTerms'] = 'You must accept our terms and accept the privacy policy';
            } else {
                delete this.errors['acceptTerms'];
            }
        }
    },

    methods: {
        submit(e) {
            let target = e.target;
            target.checkValidity();

            // If there is no validation error
            // we mark the form as succeded.
            if (this.errors.length === 0) {
                this.success = true;
            } else {
                this.success = false;
            }

            e.preventDefault();
        }
    }
});
